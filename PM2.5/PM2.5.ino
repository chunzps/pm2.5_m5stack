#include <WiFi.h>
#include <PubSubClient.h>
#include <M5Stack.h>
#include <ArduinoJson.h>
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/gpio.h"


//#define WIFI_STA_NAME "Redmi S2"
//#define WIFI_STA_PASS "11111111"
#define WIFI_STA_NAME "ChunTezuka"
#define WIFI_STA_PASS "12345678"


#define GFXFF 1
#define FF18 &FreeSans12pt7b

// Custom are fonts added to library "TFT_eSPI\Fonts\Custom" folder
// a #include must also be added to the "User_Custom_Fonts.h" file
// in the "TFT_eSPI\User_Setups" folder. See example entries.
#define CF_OL32 &Orbitron_Light_32
#define CF_OL24 &Orbitron_Light_24
#define CF_RT24 &Roboto_Thin_24

WiFiClient client;


void setup() {

  Serial.begin(115200);
  M5.begin();

  M5.Lcd.setTextDatum(MC_DATUM);
  //M5.Lcd.setBrightness(100);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setFreeFont(CF_OL32);
  M5.Lcd.setTextColor(TFT_WHITE, TFT_BLACK);
  M5.Lcd.drawString("Bangkok", 160, 40, GFXFF);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_STA_NAME);

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_STA_NAME, WIFI_STA_PASS);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");

  }


  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  WiFiClient client;
  client.setTimeout(10000);
  if (!client.connect("https://api.waqi.info", 80)) {
    Serial.println(F("Connection failed"));
    return;
  }



}

void thonburi()
{

  client.setTimeout(20000);

  if (!client.connect("https://api.waqi.info", 80)) {
    Serial.println(F("Connection failed"));
    return;
  }
  // Send HTTP request
  client.println(F("GET https://api.waqi.info/feed/thailand/bangkok/thonburi-power-sub-station/?token=b1037cebec2cc0ac1a6d46dae2dbe4da96d94841 HTTP/1.0"));
  client.println(F("Host: arduinojson.org"));
  client.println(F("Connection: close"));
  if (client.println() == 0) {
    Serial.println(F("Failed to send request"));
    return;
  }

  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    Serial.print(F("Unexpected response: "));
    Serial.println(status);
    return;
  }


  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    Serial.println(F("Invalid response"));
    return;
  }

  // Allocate JsonBuffer
  // Use arduinojson.org/assistant to compute the capacity.
  const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
  DynamicJsonBuffer jsonBuffer(capacity);

  // Parse JSON object
  JsonObject& root = jsonBuffer.parseObject(client);
  if (!root.success()) {
    Serial.println(F("Parsing failed!"));
    return;
  }

  // Extract values
  Serial.println(F("Response:"));
  Serial.println(root["status"].as<char*>());
  Serial.println(root["data"]["iaqi"]["pm25"]["v"].as<char*>());

  String pm25 = (root["data"]["iaqi"]["pm25"]["v"].as<char*>());
  String pm10 = (root["data"]["iaqi"]["pm10"]["v"].as<char*>());

  M5.Lcd.setTextDatum(MC_DATUM);

  if (pm25.toInt() <= 50) {
    M5.Lcd.fillScreen(TFT_GREEN);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_GREEN);
  }
  else if((pm25.toInt() >50) && (pm25.toInt()<=100))
  {
    M5.Lcd.fillScreen(TFT_YELLOW);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_YELLOW);
  }
  else if((pm25.toInt() >100) && (pm25.toInt()<=150))
  {
    M5.Lcd.fillScreen(TFT_ORANGE);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_ORANGE);
  }
  else
  {
    M5.Lcd.fillScreen(TFT_RED);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_RED);
  }


  M5.Lcd.setFreeFont(CF_OL32);

  M5.Lcd.drawString("Bangkok", 160, 40, GFXFF);
  M5.Lcd.setFreeFont(CF_OL24);
  M5.Lcd.drawString("Thonburi Station", 160, 80, GFXFF);
  M5.Lcd.setFreeFont(CF_OL32);
  M5.Lcd.drawString("PM2.5 :  " + pm25, 160, 140, GFXFF);
  M5.Lcd.drawString("PM10 :  " + pm10, 160, 180, GFXFF);



  M5.update();


}


void victory()
{


  if (!client.connect("http://api.waqi.info", 80)) {
    Serial.println(F("Connection failed"));
    return;
  }
  // Send HTTP request
  client.println(F("GET https://api.waqi.info/feed/thailand/bangkok/public-relations-department/?token=b1037cebec2cc0ac1a6d46dae2dbe4da96d94841 HTTP/1.0"));
  client.println(F("Host: arduinojson.org"));
  client.println(F("Connection: close"));
  if (client.println() == 0) {
    Serial.println(F("Failed to send request"));
    return;
  }

  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.0 200 OK") != 0) {
    Serial.print(F("Unexpected response: "));
    Serial.println(status);
    return;
  }

  

  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    Serial.println(F("Invalid response"));
    //return;
  }

  // Allocate JsonBuffer
  // Use arduinojson.org/assistant to compute the capacity.
  const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
  DynamicJsonBuffer jsonBuffer(capacity);

  // Parse JSON object
  JsonObject& root = jsonBuffer.parseObject(client);
  if (!root.success()) {
    Serial.println(F("Parsing failed!"));
    return;
  }

  // Extract values
  Serial.println(F("Response:"));
  Serial.println(root["status"].as<char*>());
  Serial.println(root["data"]["iaqi"]["pm25"]["v"].as<char*>());

  String pm25B = (root["data"]["iaqi"]["pm25"]["v"].as<char*>());
  String pm10B = (root["data"]["iaqi"]["pm10"]["v"].as<char*>());


  M5.Lcd.setTextDatum(MC_DATUM);

  if (pm25B.toInt() <= 50) {
    M5.Lcd.fillScreen(TFT_GREEN);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_GREEN);
  }
  else if((pm25B.toInt() >50) && (pm25B.toInt()<=100))
  {
    M5.Lcd.fillScreen(TFT_YELLOW);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_YELLOW);
  }
  else if((pm25B.toInt() >100) && (pm25B.toInt()<=150))
  {
    M5.Lcd.fillScreen(TFT_ORANGE);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_ORANGE);
  }
  else
  {
    M5.Lcd.fillScreen(TFT_RED);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_RED);
  }

  M5.Lcd.setFreeFont(CF_OL32);

  M5.Lcd.drawString("Bangkok", 160, 40, GFXFF);
  M5.Lcd.setFreeFont(CF_OL24);
  M5.Lcd.drawString("PR Department", 160, 80, GFXFF);
  M5.Lcd.setFreeFont(CF_OL32);
  M5.Lcd.drawString("PM2.5 :  " + pm25B, 160, 140, GFXFF);
  M5.Lcd.drawString("PM10 :  " + pm10B, 160, 180, GFXFF);



  M5.update();


}

void chulalongkorn()
{



  if (!client.connect("http://api.waqi.info", 80)) {
    Serial.println(F("Connection failed"));
    return;
  }
  // Send HTTP request
  client.println(F("GET https://api.waqi.info/feed/thailand/bangkok/chulalongkorn-hospital/?token=b1037cebec2cc0ac1a6d46dae2dbe4da96d94841 HTTP/1.0"));
  client.println(F("Host: arduinojson.org"));
  client.println(F("Connection: close"));
  if (client.println() == 0) {
    Serial.println(F("Failed to send request"));
    //return;
  }


  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.0 200 OK") != 0) {
    Serial.print(F("Unexpected response: "));
    Serial.println(status);
    return;
  }


  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    Serial.println(F("Invalid response"));
    return;
  }
  

  // Allocate JsonBuffer
  // Use arduinojson.org/assistant to compute the capacity.
  const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
  DynamicJsonBuffer jsonBuffer(capacity);

  // Parse JSON object
  JsonObject& root = jsonBuffer.parseObject(client);
  if (!root.success()) {
    Serial.println(F("Parsing failed!"));
    return;
  }

  // Extract values
  Serial.println(F("Response:"));
  Serial.println(root["status"].as<char*>());
  Serial.println(root["data"]["iaqi"]["pm25"]["v"].as<char*>());

  String pm25C = (root["data"]["iaqi"]["pm25"]["v"].as<char*>());
  String pm10C = (root["data"]["iaqi"]["pm10"]["v"].as<char*>());


  M5.Lcd.setTextDatum(MC_DATUM);

  if (pm25C.toInt() <= 50) {
    M5.Lcd.fillScreen(TFT_GREEN);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_GREEN);
  }
  else if((pm25C.toInt() >50) && (pm25C.toInt()<=100))
  {
    M5.Lcd.fillScreen(TFT_YELLOW);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_YELLOW);
  }
  else if((pm25C.toInt() >100) && (pm25C.toInt()<=150))
  {
    M5.Lcd.fillScreen(TFT_ORANGE);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_ORANGE);
  }
  else
  {
    M5.Lcd.fillScreen(TFT_RED);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_RED);
  }


  M5.Lcd.setFreeFont(CF_OL32);

  M5.Lcd.drawString("Bangkok", 160, 40, GFXFF);
  M5.Lcd.setFreeFont(CF_OL24);
  M5.Lcd.drawString("Chula Hospital", 160, 80, GFXFF);
  M5.Lcd.setFreeFont(CF_OL32);
  M5.Lcd.drawString("PM2.5 :  " + pm25C, 160, 140, GFXFF);
  M5.Lcd.drawString("PM10 :  " + pm10C, 160, 180, GFXFF);



  M5.update();


}

void here()
{

  if (!client.connect("https://api.waqi.info", 80)) {
    Serial.println(F("Connection failed"));
    return;
  }
  // Send HTTP request
  client.println(F("GET https://api.waqi.info/feed/here/?token=b1037cebec2cc0ac1a6d46dae2dbe4da96d94841 HTTP/1.0"));
  client.println(F("Host: arduinojson.org"));
  client.println(F("Connection: close"));
  if (client.println() == 0) {
    Serial.println(F("Failed to send request"));
    return;
  }


  // Check HTTP status
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    Serial.print(F("Unexpected response: "));
    Serial.println(status);
    return;
  }

  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    Serial.println(F("Invalid response"));
    return;
  }
  

  // Allocate JsonBuffer
  // Use arduinojson.org/assistant to compute the capacity.
  const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
  DynamicJsonBuffer jsonBuffer(capacity);

  // Parse JSON object
  JsonObject& root = jsonBuffer.parseObject(client);
  if (!root.success()) {
    Serial.println(F("Parsing failed!"));
    return;
  }

  // Extract values
  Serial.println(F("Response:"));
  Serial.println(root["status"].as<char*>());
  Serial.println(root["data"]["iaqi"]["pm25"]["v"].as<char*>());

  String pm25C = (root["data"]["iaqi"]["pm25"]["v"].as<char*>());
  String pm10C = (root["data"]["iaqi"]["pm10"]["v"].as<char*>());


  M5.Lcd.setTextDatum(MC_DATUM);
  
  

  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setFreeFont(CF_OL32);
  M5.Lcd.setTextColor(TFT_WHITE, TFT_BLACK);
  M5.Lcd.drawString("Bangkok", 160, 40, GFXFF);
  M5.Lcd.setFreeFont(CF_OL24);
  M5.Lcd.drawString("Bangkunnon", 160, 80, GFXFF);
  M5.Lcd.setFreeFont(CF_OL32);
  M5.Lcd.drawString("PM2.5 :  " + pm25C, 160, 140, GFXFF);
  M5.Lcd.drawString("PM10 :  " + pm10C, 160, 180, GFXFF);



  M5.update();

}


void loop()
{


  thonburi();
  delay (20000);
  victory();
  delay (20000);
  chulalongkorn();
  delay (20000);

}
